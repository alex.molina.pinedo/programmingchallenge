// challenge.cpp : Este archivo contiene la función "main". La ejecución del programa comienza y termina ahí.
//

#include <iostream>
#include <fstream>

float mortgage = 10000.f;
int toPay = 375;
int additionalPay = 500;
float interestRate = 0.005f;
int month = 1;

std::ofstream csvFile;

void finalFormatData(int m, float interest, float mort, float dueInterest, int finalToPay)
{
    if (m <= 9)
    {
        std::cout << "Month " << m << "\t\t";
    }
    else
    {
        std::cout << "Month " << m << "\t";
    }  
    std::cout << "Interest " << interest << "\t\t";
    if (mort < 1000)
    {
        std::cout << "Due " << mort << "\t\t\t";
    }
    else
    {
        std::cout << "Due " << mort << "\t\t";
    }   
    std::cout << "Due + Interest " << dueInterest << "\t\t";
    std::cout << "To Pay " << finalToPay << std::endl;

    csvFile << m << "," << interest << "," << mort << "," << dueInterest << "," << finalToPay << "\n";
}

void printResultMonthly(int month)
{
    float interest = mortgage * interestRate;
    float dueInterest = mortgage + interest;
    
    if (month == 12)
    {
        finalFormatData(month, interest, mortgage, dueInterest, toPay + additionalPay);

        mortgage -= additionalPay;
        month = 1;
    }
    else
    {
        finalFormatData(month, interest, mortgage, dueInterest, toPay);

        month++;
    }

    mortgage -= toPay;

    if (mortgage > 0)
    {
        printResultMonthly(month);
    }
}

int main()
{
    csvFile.open("test.csv");
    
    printResultMonthly(month);
    
    csvFile.close();
    return 0;
}
